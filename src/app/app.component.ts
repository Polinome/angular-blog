import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  posts = [
    {
      title: 'First post scarrrrry !',
      content: 'Votre AppComponent contiendra l\'array des posts, et il le passera à un component PostListComponent',
      date: new Date(2018, 2, 3, 12, 13, 10),
      likes: 0
    },
    {
      title: 'Let me tell you a secret...',
      content: 'Votre PostListComponent affichera un PostListItemComponent pour chaque post dans l\'array qu\'il a reçu',
      date: new Date(2019, 6, 13, 15, 0, 32),
      likes: 6
    },
    {
      title: 'Bootstrap is cool...',
      content: 'Contextual classes also work with .list-group-item-action. Note the addition of the hover styles here not present in the previous example. Also supported is the .active state; apply it to indicate an active selection on a contextual list group item.',
      date: new Date(2019, 6, 13, 15, 0, 32),
      likes: -16
    }
  ];
}
