import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.sass']
})
export class PostListComponent implements OnInit {
  @Input() introduction: string;

  @Input() items: object = [];

  constructor() { }

  ngOnInit() {
    console.log(this.items);
  }

}
